const path = require("path");
  module.exports = {
    entry: ["./frontend/src/js/app.js"],
    output: {
      path: path.resolve(__dirname, "frontend"),
      filename: "web/js/[name].js"
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    }
  };