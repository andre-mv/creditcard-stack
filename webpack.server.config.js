const path = require("path");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

  module.exports = {
    name: 'server',
    target: 'node',
    plugins: [
      new UglifyJSPlugin({
        sourceMap: false,
        uglifyOptions: {
          mangle: false,
        }
      }),
    ],
    entry: ["./backend/index.js"],
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "server.js"
    },
    externals: [nodeExternals()],
    module: {
      rules: [
        {
          test: [/\.js$|.jsx$/],
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              plugins: [
                ['transform-runtime'],
              ]
            }
          },
          
        }

      ]
    }
  };


