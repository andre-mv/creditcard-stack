

const path = require("path");
const webpack = require('webpack');

  module.exports = {
    entry: ["./frontend/src/js/app.js"],
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "js/[name].js"
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        SOCKET_URL: JSON.stringify(process.env.SOCKET_URL ? process.env.SOCKET_URL : 'wss://localhost:3000'),
      })
    ]
  };

