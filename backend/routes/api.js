import Controller from '../controllers/api';

export default [
  {
    method: 'GET',
    path: '/cards',
    config: Controller.get,
  }
];
