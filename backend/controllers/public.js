export default {
  css: {
    handler: {
      directory: { path: 'dist/public/css' },
    },
  },
  js: {
    handler: {
      directory: { path: 'dist/public/js' },
    },
  },
  images: {
    handler: {
      directory: { path: 'dist/public/images' },
    },
  },
};
