import Moment from 'moment';
import HttpStatus from 'http-status';
import CacheServiceImport from './../services/cacheService';

import mysql from 'mysql';

class Database {
  constructor( config ) {
      this.connection = mysql.createConnection( config );
  }
  query( sql, args ) {
      return new Promise( ( resolve, reject ) => {
          this.connection.query( sql, args, ( err, rows ) => {
              if ( err )
                  return reject( err );
              resolve( rows );
          } );
      } );
  }
  close() {
      return new Promise( ( resolve, reject ) => {
          this.connection.end( err => {
              if ( err )
                  return reject( err );
              resolve();
          } );
      } );
  }
}

const cacheService = CacheServiceImport();
const socketPrefix = 'cards';

const badRequestResponse = reply =>
    reply({
        'bad request': 'false'
    }).code(HttpStatus.BAD_REQUEST);


export default {
    get: {
        handler: (request, reply) => {

            var resultados;

            var config = {
              host: "localhost",
              user: "root",
              password: "",
              database: "creditcards"
            };

            var database = new Database(config);

            return database.query( 'SELECT id, name, number, cvv, expiration, company FROM cards' ).then( rows => {
              
              resultados = rows;
              
            }).then( () => {
              console.log(resultados);

              return {'store':resultados, 'responseCode':1};
            });

        },
        description: 'Get cards',
        notes: 'Gets the cards',
        tags: ['api'],
    },
    
};