/* eslint-disable import/no-unresolved */

import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import HttpStatus from 'http-status';
import DecodeHtml from 'decode-html';
import { Router } from 'react-router';

import CacheServiceImport from './../services/cacheService';

import configureStore from './../../frontend/src/js/store/index';

import {loadCards} from './../../frontend/src/js/actions/index';

import routes from './../../frontend/src/js/routes';

import { StaticRouter } from 'react-router-dom/StaticRouter';

import Index from './../../frontend/src/js/components/Index';

const store = configureStore();

const loggerMiddleware = createLogger();

const cacheService = CacheServiceImport();

export default {
  index: {
 
    handler: (request, reply) => {
      const cards = cacheService.get();

      const reduxStore = (
        <Provider store={store}>
          <Router history={request.url} routes={routes}/>
        </Provider>
      );
      const cardComponent = ReactDOMServer.renderToString(routes);
      
      const indexComponent = <Index cards={cardComponent} />;

      let indexPage = DecodeHtml(
        ReactDOMServer.renderToStaticMarkup(reduxStore),
      );
      indexPage = indexPage.replace(/&quot;/gi, '"');

      return indexPage;
    },
  },
};
