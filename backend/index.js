'use strict';

const Hapi = require('hapi');

import Routes from './routes/all';

const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});

const init = async () => {

    await server.register(require('inert'));

    server.route(Routes.Public);
    server.route(Routes.Index);
    server.route(Routes.Api);

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();