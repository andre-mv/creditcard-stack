import * as types from "../constants/action-types";
import cardsApi from '../api/CardsApi';

export function loadCardsType(cards) {
  return {type: types.LOAD_CARDS, cards};
}

export function updateCardType(card) {
  return {type: types.UPDATE_CARD, card}
}

export function createCardType(card) {
  return {type: types.CREATE_CARD, card}
}

export function deleteCardType(card) {
  return {type: types.DELETE_CARD, card}
}

export function loadCards() {
  return function(dispatch) {
    return cardsApi.getAllCards().then(cards => {
      dispatch(loadCardType(cards));
    }).catch(error => {
      throw(error);
    });
  };
}

export function updateCard(card) {
  return function (dispatch) {
    return cardsApi.updateCard(card).then(responseCard => {
      dispatch(updateCardType(responseCard));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createCard(card) {
  return function (dispatch) {
    return cardsApi.createCard(card).then(responseCard => {
      dispatch(createCardType(responseCard));
      return responseCard;
    }).catch(error => {
      throw(error);
    });
  };
}

export function deleteCard(card) {
  return function(dispatch) {
    return cardsApi.deleteCard(card).then(() => {
      console.log(`Deleted ${card.id}`)
      dispatch(deleteCardType(card));
      return;
    }).catch(error => {
      throw(error);
    })
  }
}