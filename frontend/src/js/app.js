import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/index';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import {loadCards} from './actions/index';

  
const store = configureStore();

store.dispatch(loadCards());

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('app')
);
