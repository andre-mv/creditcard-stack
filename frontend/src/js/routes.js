import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import CardsPage from './components/CardsPage';
import CardPage from './components/CardPage';
import NewCardPage from './components/NewCardPage';

export default (
  <Route path="/" component={App}>
    <Route path="/cards" component={CardsPage}>
      <Route path="/cards/new" component={NewCardPage} />
      <Route path="/cards/:id" component={CardPage} />
    </Route>
  </Route>
);
