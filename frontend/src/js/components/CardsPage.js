import React from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import CardList from './CardList';
import NewCardPage from './NewCardPage';
import * as actions from '../actions/index';

import PropTypes from 'prop-types';

class CardsPage extends React.Component {
  componentWillMount() {
    if (this.props.cards[0].id == '') {
      this.props.actions.loadCards();
    }
  }
  render() {
    const cards = this.props.cards;
    return (
      <div className="col-md-12">
        <h1>Cartões <Link to={'/cards/new'} className="btn btn-primary">Adicionar Cartão</Link></h1>
        <div className="col-md-4">
          <CatList cards={cards} />
        </div>
        <div className="col-md-8">
          {this.props.children}
        </div>
      </div>
    );
  }
}

CardsPage.propTypes = {
  cards: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  if (state.cards.length > 0) {
    return {
      cards: state.cards
    };
  } else {
    return {
      cards: [{id: '', name: '', number: '', expiration: '', cvv: ''}]
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(CardsPage);





