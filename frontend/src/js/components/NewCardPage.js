import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
import * as courseActions from '../actions/index';
import CardForm from './CardForm';

import PropTypes from 'prop-types';


class NewCardPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      card: {name: '', number: '', expiration: '', cvv: ''},
      saving: false
    };
    this.saveCard = this.saveCard.bind(this);
    this.updateCardState = this.updateCardState.bind(this);
  }

  updateCardState(event) {
    const field = event.target.name;
    const card = this.state.card;
    card[field] = event.target.value;
    return this.setState({card: card});
  }

  saveCard(event) {
    event.preventDefault();
    this.props.actions.createCard(this.state.card)
  }
  
  render() {
    return (
      <div>
        <h1>new card</h1>
        <CardForm 
          card={this.state.card} 
          onSave={this.saveCard}
          onChange={this.updateCardState}/>
      </div>
    );
  }
}

NewCardPage.propTypes = {
  actions: PropTypes.object.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}


export default connect(mapDispatchToProps)(NewCardPage);





