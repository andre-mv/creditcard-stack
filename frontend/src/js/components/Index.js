import React from 'react';
import PropTypes from 'prop-types';

const Index = props => {
  const { cards } = props;

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <title>
          Creditcards CRUD
        </title>
        <meta
          name="description"
          content="Creditcards CRUD."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"/>
        <style dangerouslySetInnerHTML={{__html: `
            body
            {
            font-family: 'Montserrat', sans-serif;
            }

            h2
            {
            color: #505050;
            text-align: center;
            }

            .margint20
            {
            margin-top: 20px;
            }

            .block
            {
            display: block;
            }


            .box
            {
            background-color: rgb(240, 240, 240);
            padding: 20px;
            }

            .form-control
            {
            padding: 5px 5px 5px 5px;
            margin: 10px 0;
            font-family: sans-serif;
            }

            .btn-save {
            color: #fff;
            background-color: #4b20ad;
            border-color: #3e20aa;
            }

            .container-creditcard
            {
            min-height: 300px;
            width: 454px;
            margin: auto;
            }

            .box-creditcard
            {
            position: absolute;
            backface-visibility: hidden;

            }



            .flip .flipper, .flip .flipper {
            transform: rotateY(180deg);
            }

            .flipper {
            transition: 0.6s;
            transform-style: preserve-3d;
            position: relative;
            }

            .creditcard-front {
            z-index: 2;
            transform: rotateY(0deg);
            }

            

            .creditcard-back {
            transform: rotateY(180deg);
            }

            .number-card
            {
            position: absolute;
            left: 30px;
            top: 110px;
            color: white;
            font-size: 2.1em;
            letter-spacing: 7px;
            }

            .name-card
            {
            position: absolute;
            left: 20px;
            top: 180px;
            color: white;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 306px;
            }

            .company-card
            {
            position: absolute;
            right: 60px;
            top: 30px;

            }

            .expiration-card {
            position: absolute;
            left: 337px;
            top: 198px;
            color: white;
            font-size: 1.5em;
            }

            .cvv-card
            {
            position: absolute;
            right: 20px;
            top: 110px;
            color: white;
            font-size: 1.5em;
            }
        `}} />

        
      </head>
      <body>
        <main>
          <div id="app">{cards}</div>
        </main>
        <script async src="/js/main.js" />
      </body>
    </html>
  );
};

Index.propTypes = {
  cards: PropTypes.string.isRequired,
};

export default Index;
