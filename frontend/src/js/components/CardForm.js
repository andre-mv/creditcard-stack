import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addCard } from "../actions/index";
import MaskInput from 'react-maskinput';


class CardForm extends Component {
  constructor(props) {
	super(props);

	this.state = {
	  numberCard:     "",
	  nameCard:       "",
	  expirationCard: "",
	  cvvCard:        ""
	};

	this.handleChange = this.handleChange.bind(this);
	this.handleSubmit = this.handleSubmit.bind(this);
	this.onFocus      = this.onFocus.bind(this);
	this.onBlur      = this.onBlur.bind(this);
  }

  companyChange(numberCard) {
	const regexVisa = /^4/;
	const regexMaster = /^5[1-5]{1}/;
	const regexAmex = /^3[47]{1}/;
	const regexElo = /^(?:40117[8-9]|431274|438935|451416|457393|45763[1-2]|504175|627780|636297|636368|65500[0-1]|65165[2-4]|65048[5-8]|506699|5067[0-6]\d|50677[0-8]|509\d{3})\d{10}$/;
	const invalidos = /^[0126789]|^5[06-9]{1}|^3[1235689]/;

	if(regexVisa.test(numberCard))
	{
	  return "images/visa.png";
	}

	else if(regexMaster.test(numberCard))
	{
	  return "images/mastercard.png";
	}

	else if(regexAmex.test(numberCard))
	{
	  return "images/amex.png";
	}

	else if(regexElo.test(numberCard))
	{
	  return "images/elo.png";
	}

	else if(invalidos.test(numberCard))
	{
	  return false;
	}

	
  }

  handleChange(event) {
	this.setState({ [event.target.id]: event.target.value });
  }

  onFocus(event) {
	this.setState({ focusedCVV: true});
  }

  onBlur(event) {
	this.setState({ focusedCVV: false});
  }

  handleSubmit(event) {
	event.preventDefault();
	const { numberCard , nameCard , expirationCard , cvvCard } = this.state;
	const id = Math.floor(Math.random() * 6) + 1;
	this.props.addCard({ id , numberCard , nameCard , expirationCard , cvvCard});
	this.setState({ numberCard:     "", 
									nameCard:       "", 
									expirationCard: "", 
									cvvCard:        "" });
  }

  //ajustar css para vários tamanhos de tela

  render() {
	const { numberCard , nameCard , expirationCard , cvvCard } = this.state;
	const defaultNameCard = "NOME COMPLETO";
	const defaultExpirationCard = "••/••";
	


	return (
	  <form onSubmit={this.handleSubmit}>
		<div className="form-group margint20">
		<div className={this.state.focusedCVV ? 'container-creditcard flip' : 'container-creditcard'}>
		  <div className="flipper">
			<div className="box-creditcard creditcard-front">
			  <img src="images/creditcard_front.png" width="100%"/>
			  <span className="number-card">{numberCard}</span>
			  <img className="company-card" src={this.companyChange(numberCard)}/>
			  <span className="name-card">{nameCard ? nameCard.toUpperCase() : defaultNameCard}</span>
			  <span className="expiration-card">{expirationCard ? expirationCard : defaultExpirationCard}</span>
			</div>
			<div className="box-creditcard creditcard-back">
			  <img src="images/creditcard_back.png" width="100%"/>
				<span className="cvv-card">{cvvCard}</span>
			  
			</div>
		  </div>
		  </div>
		  <MaskInput
			placeholder='Número do cartão'
			maskChar=''
			id='numberCard'
			mask='0000 0000 0000 0000'
			className="form-control"
			value={numberCard}
			onChange={this.handleChange}       
		  />
		  <input
			maxLength = "32"
			placeholder="Nome"
			type="text"
			className="form-control"
			id="nameCard"
			value={nameCard}
			onChange={this.handleChange}
		  />
		  <MaskInput
			placeholder='MM/YY'
			maskChar=''
			id='expirationCard'
			mask='00/00'
			className="form-control"
			value={expirationCard}
			onChange={this.handleChange}       
		  />

			<MaskInput
			placeholder='CVV'
			maskChar=''
			id='cvvCard'
			mask='000'
			className="form-control"
			value={cvvCard}
			onChange={this.handleChange}  
			onFocus = { this.onFocus }
			onBlur = { this.onBlur }     
		  />
		</div>

		


		<button type="submit" className="btn btn-save btn-lg">
		  Salvar
		</button>
	  </form>
	);
  }
}

CardForm.propTypes = {
  card: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool
};

export default CardForm;


