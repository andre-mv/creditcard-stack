import * as types from "../constants/action-types";
import initialState from './initialState';
import {browserHistory} from 'react-router';


export default function cardReducer(state = initialState.cards, action) {
  switch(action.type) {
    case types.LOAD_CARDS:
     return action.cards
    case types.CREATE_CARD:
      browserHistory.push(`/cards/${action.card.id}`)
      return [
        ...state.filter(card => card.id !== action.card.id),
        Object.assign({}, action.card)
      ]
    case types.UPDATE_CARD:
      return [
        ...state.filter(card => card.id !== action.card.id),
        Object.assign({}, action.card)
      ]
    case types.DELETE_CARD: {
      const newState = Object.assign([], state);
      const indexOfCardToDelete = state.findIndex(cat => {return card.id == action.card.id})
      newState.splice(indexOfCardToDelete, 1);
      browserHistory.push('/cards');
      return newState;
    }
    default: 
      return state;
  }
}
