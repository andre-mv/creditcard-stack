export const LOAD_CARDS = 'LOAD_CARDS';
export const UPDATE_CARD = 'UPDATE_CARD';
export const CREATE_CARD = 'CREATE_CARD';
export const DELETE_CARD = 'DELETE_CARD';