class CardsApi {

  static getAllCards() {
    const request = new Request(`http://localhost:3000/cards`, {
      method: 'GET'
    });

    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static updateCard(card) {
    const headers = {'Content-Type': 'application/json'};
    const request = new Request(`http://localhost:3000/cards/${card.id}`, {
      method: 'PUT',
      headers: headers, 
      body: JSON.stringify({card: card})
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static createCard(card) {
    const headers = {'Content-Type': 'application/json'};
    const request = new Request(`http://localhost:3000/cards`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({card: card})
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static deleteCard(card) {
    const headers = {'Content-Type': 'application/json'};
    const request = new Request(`http://localhost:3000/cards/${card.id}`, {
      method: 'DELETE', 
      headers: headers
    });

    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }
}

export default CardsApi;
