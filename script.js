var mysql = require('mysql');

var connection_before = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: ""
});

var connection_after = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "creditcards"
});

function createDB(connection){

    connection.query("CREATE DATABASE IF NOT EXISTS creditcards", function(error, result) {
        if (error) throw error;
        console.log("Database created");
    });
}

function createTable(connection){

    const sql = "CREATE TABLE IF NOT EXISTS cards (\n" +
    "id int NOT NULL AUTO_INCREMENT,\n" +
    "name varchar(150) NOT NULL,\n" +
    "number bigint(16) NOT NULL,\n" +
    "cvv int(3) NOT NULL,\n" +
    "expiration date NOT NULL,\n" +
    "company varchar(150) NOT NULL,\n" +
    "PRIMARY KEY (ID)\n" +
    ");";


    connection.query(sql, function(error, results, fields) {
        if (error) throw error;
        console.log('Table Creditcards created');
    });
}

function addRows(conn){
    const sql = "INSERT INTO cards(name,number,cvv,expiration,company) VALUES ?";
    const values = [
          ['teste1', '1234567891234567', '123', '2021-09-01', 'mastercard'],
          ['teste1', 1234567891234567, '123', '2021-09-01', 'mastercard'],
          ['teste3', 1234567891234567, '123', '2021-09-01', 'mastercard']
        ];
    conn.query(sql, [values], function (error, results, fields){
            if(error) return console.log(error);
            console.log('adicionou registros!');
            conn.end();
        });
  }

connection_before.connect(function(error) {
    if (error) throw error;
    console.log("Connected!");
    createDB(connection_before);
});


connection_after.connect(function(error) {
    if (error) throw error;
    console.log("Connected!");
    createTable(connection_after);

    //addRows(connection_after);
});